static const AVCodec * const codec_list[] = {
    &ff_h263_decoder,
    &ff_h264_decoder,
    &ff_hevc_decoder,
    &ff_mjpeg_decoder,
    &ff_mpeg4_decoder,
    NULL };
