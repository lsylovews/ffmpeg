static const AVCodecParser * const parser_list[] = {
    &ff_h263_parser,
    &ff_h264_parser,
    &ff_hevc_parser,
    &ff_mjpeg_parser,
    &ff_mpeg4video_parser,
    &ff_mpegvideo_parser,
    NULL };
